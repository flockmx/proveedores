<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\Object;

/**
 * LoginForm is the model behind the login form.
 */
class Login extends \yii\base\Object
{
    public $email;
    public $name;

    private $_user = false;


    public function __construct( $email, $name ) {

        $this->_user = Usuario::find()->where(['email' => $email])->one();

        if( $this->_user === null ){

            $this->_user = new Usuario;
            $this->_user->nombre = $name;
            $this->_user->apellido = $email;
            $this->_user->email = $email;
            $this->_user->tipo = 2;
            
            if( !$this->_user->save() ){
                $this->_user = null;
            }
        }

    }


    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ( $this->_user !== null ) {

            if( $this->_user->tipo_proveedores ===  null ){
                return null;
            }


            return Yii::$app->user->login( $this->getUser(), 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->_user;
    }
}
