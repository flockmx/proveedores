<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CertificacionesProveedor".
 *
 * @property integer $id
 * @property integer $id_proveedor
 * @property integer $id_certificacion
 *
 * @property Proveedor $idProveedor
 * @property Certificacion $idCertificacion
 */
class CertificacionesProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CertificacionesProveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_proveedor', 'id_certificacion'], 'required'],
            [['id', 'id_proveedor', 'id_certificacion'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_proveedor' => 'Id Proveedor',
            'id_certificacion' => 'Id Certificacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['id' => 'id_proveedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificacion()
    {
        return $this->hasOne(Certificacion::className(), ['id' => 'id_certificacion']);
    }
}
