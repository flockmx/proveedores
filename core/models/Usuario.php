<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Usuario".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property string $email
 * @property integer $tipo
 * @property integer $id_area
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{   

    const USER_ADMIN = 1;
    const USER_SIMPLE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo', 'id_area'], 'integer'],
            [['nombre', 'apellido', 'email'], 'string', 'max' => 45],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'email' => 'Email',
            'tipo' => 'Tipo',
            'id_area' => 'Id Area',
        ];
    }


    public static function isUserAdmin( $email ){

        if( Usuario::find()->where(['email' => $email, 'tipo_proveedores' => Usuario::USER_ADMIN])->one() ){
            return true;
        }
            
        return false;
    }

    public static function isUserSimple( $email ){

        if( Usuario::find()->where(['email' => $email, 'tipo_proveedores' => Usuario::USER_SIMPLE])->one() ){
            return true;
        }
            
        return false;
    }



    /**
     * @inheritdoc
     */
    public static function findIdentity($email){
        return Usuario::find()->where(['email' => $email])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return Usuario::find()->where(['id' => $token])->one();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->email;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->id === $authKey;
    }
}
