<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Proveedor".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property string $contacto
 * @property string $telefono
 * @property string $correo
 * @property integer $creado
 * @property integer $actualizado
 *
 * @property CertificacionesProveedor[] $certificacionesProveedors
 * @property SkillProveedor[] $skillProveedors
 * @property ProveedorCategoria[] $proveedorCategorias
 */
class Proveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'contacto', 'telefono', 'correo'], 'required'],
            [['creado', 'actualizado'], 'integer'],
            [['nombre', 'contacto', 'correo'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255],
            [['telefono'], 'string', 'max' => 20],
            ['correo', 'email'],
            ['nombre', 'unique'],

        ];
    }

     public function beforeSave($insert) {
        
        
        if ($this->isNewRecord){
            $this->creado = time();
        }
            
        
        $this->actualizado = time();
     
        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     */


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'razonsocial' => 'Razón Social',
            'descripcion' => 'Descripción',
            'contacto' => 'Nombre de Contacto',
            'telefono' => 'Teléfono',
            'telefonocel' => 'Teléfono Celular',
            'correo' => 'Correo',
            'creado' => 'Creado',
            'actualizado' => 'Actualizado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificaciones()
    {
        return $this->hasMany(CertificacionesProveedor::className(), ['id_proveedor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkills()
    {
        return $this->hasMany(SkillProveedor::className(), ['id_proveedor' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias()
    {
        return $this->hasMany(ProveedorCategoria::className(), ['id_proveedor' => 'id']);
    }
}
