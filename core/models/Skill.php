<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Skill".
 *
 * @property integer $id
 * @property string $skill
 * @property string $descripcion
 *
 * @property SkillProveedor[] $skillProveedors
 */
class Skill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Skill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['skill'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'skill' => 'Skill',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkillProveedors()
    {
        return $this->hasMany(SkillProveedor::className(), ['id_skill' => 'id']);
    }
}























