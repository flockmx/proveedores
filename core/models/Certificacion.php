<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Certificacion".
 *
 * @property integer $id
 * @property string $certificacion
 * @property string $descripcion
 *
 * @property CertificacionesProveedor[] $certificacionesProveedors
 */
class Certificacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Certificacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['certificacion'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'certificacion' => 'Certificacion',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificaciones()
    {
        return $this->hasMany(CertificacionesProveedor::className(), ['id_certificacion' => 'id']);
    }
}
