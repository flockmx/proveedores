<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SkillProveedor".
 *
 * @property integer $id
 * @property integer $id_skill
 * @property integer $id_proveedor
 *
 * @property Proveedor $idProveedor
 * @property Skill $idSkill
 */
class SkillProveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SkillProveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_skill'], 'required'],
            [['id_skill', 'id_proveedor'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_skill' => 'Id Skill',
            'id_proveedor' => 'Id Proveedor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['id' => 'id_proveedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkill()
    {
        return $this->hasOne(Skill::className(), ['id' => 'id_skill']);
    }
}
