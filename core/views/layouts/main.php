<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <script>

        var ajaxUrl = '<?php echo Yii::getAlias('@web'); ?>';

    </script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '  ',
        'brandUrl' => '/site/inicio',
        'options' => [
            'class' => '',
        ],
    ]);


   /* echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Proveedores', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/site/login']] :
                [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
        ],
    ]);*/
?>

<ul class="nav navbar-nav navbar-right" id="yw0">
<li><a href="/site/inicio">Inicio</a></li>
<li><a href="/brief/index">Brief Tool</a></li>
<li><a href="/site/timesheet">Timesheet</a></li>

<li class="active"><a href="/proveedores/proveedor/comparativo">Proveedores</a></li>


<li><a class="btn-logout" href="/site/logout">Logout</a></li>
<li><a href="#"><?php echo Yii::$app->user->id; ?></a></li>

</ul>                       

<?php


    NavBar::end();
    ?>

    <?= $content ?>
          
        </footer>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
