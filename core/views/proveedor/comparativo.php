
<?php
  use yii\helpers\Html;
  use app\models\Usuario;
  use yii\widgets\LinkPager;
?>
<?php
	$this->registerJsFile( Yii::getAlias('@web').'/js/main.js', ['depends' => [\yii\web\JqueryAsset::className(), \yii\jui\JuiAsset::className(), \yii\bootstrap\BootstrapPluginAsset::className(), \yii\web\YiiAsset::className() ]]);
?>


 

<div class="container" >


	<div class="row">

   		<div class="col-md-12">
	
			<?php if( Usuario::isUserAdmin( Yii::$app->user->id ) ): ?>
				<?= Html::a('Agregar Proveedor', ['create'], ['class' => 'btn btn-primary btn-lg pull-right'])?>
			<?php endif; ?>
			
		</div>
		<br/><br/><br/>
	</div>

	<div class="row">

		<form>

			<div class="col-md-4">
				<div class="input-group">
		          <span class="input-group-addon">Nombre:</span>
		          <input type="text" class="form-control" name="nombre_proveedor" id="nombre_proveedor" value="<?= (isset($_GET['nombre_proveedor']))?$_GET['nombre_proveedor']:'' ?>" />
		        </div>	
			</div>


			<div class="col-md-4">
				<div class="input-group">
		          <span class="input-group-addon">Categoría:</span>
		          <input type="text" class="form-control" name="categorias" id="categorias" value="" />
		        </div>	
			</div>


			<div class="col-md-4">
				<div class="input-group">
		          <span class="input-group-addon">Skills:</span>
		          <input type="text" class="form-control" name="skills" id="skills" value="" />
		        </div>	
			</div>

			

			<div class="col-md-2 col-md-offset-10">
				
				<input type="submit" class="btn btn-default btn-search" value="Buscar"/>
			</div>

		</form>


    </div>

   	

	<div class="row">

		<div class="col-md-12 proveedores">

			<ul>


				<?php foreach( $models as $proveedor ): ?>

					<?php echo $this->render('proveedor', ['proveedor'=>$proveedor]); ?>

				<?php endforeach; ?>
			</ul>
		  	
		</div>
	</div>
	<div class="row">
		<?php
			// display pagination
			echo LinkPager::widget([
			    'pagination' => $pages,
			]);
		?>
	</div>

</div>
