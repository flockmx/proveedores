<?php
  use yii\helpers\Html;
  use app\models\Usuario;
?>



<li>
<div class="panel panel-default panel-proveedor">
  <div class="panel-heading">
    <h3 class="panel-title"><?= $proveedor->nombre ?> - <?=  $proveedor->razonsocial ?></h3>
  </div>
  <div class="panel-body">
    
      <table class="table table-striped ">
        <thead>
          <tr>
            <th>Skill</th>
            <th>Certificación</th>
            <th>Categoría</th>
            <th>Contácto</th>
            <th>Teléfono</th>
            <th>Celular</th>
            <th>Correo</th>
          </tr>
        </thead>
      <tbody>
         <tr>
          <td>
            <ul class="list-group">
             <?php foreach( $proveedor->skills as $skillproveedor ): ?>
                 <li class="list-group-item"><?=$skillproveedor->skill->skill?></li>
              <?php endforeach; ?>
            </ul>
          </td>
          <td>
          <ul class="list-group">
              <?php foreach( $proveedor->certificaciones as $certificacionproveedor ): ?>
                <li class="list-group-item"><?= $certificacionproveedor->certificacion->certificacion ?></li>
              <?php endforeach; ?>
            </ul>
          </td>
          <td>
          <ul class="list-group">
            <?php foreach( $proveedor->categorias as $proveedorcategoria ): ?>
              <li class="list-group-item"><?= $proveedorcategoria->categoria->Categoria ?></li>
            <?php endforeach; ?>
            </ul>

          </td>
          
          <td> <li  class="list-group-item"> <?= $proveedor->contacto ?></li></td>
          <td><li  class="list-group-item"><?= $proveedor->telefono ?> </li></td>
          
           <td><li  class="list-group-item"> <?= $proveedor->telefonocel ?></li></td>
          <td><?= $proveedor->correo?></td>
          <td><?php if( Usuario::isUserAdmin( Yii::$app->user->id ) ): ?>
          <?= Html::a('', ['update', 'id'=>$proveedor->id], ['class' => 'glyphicon glyphicon-pencil'])?>
          <?php endif; ?></td>

          <td><?php if( Usuario::isUserAdmin( Yii::$app->user->id ) ): ?>
          <?= Html::a('', ['delete', 'id' => $proveedor->id], ['class' => 
              'glyphicon glyphicon-trash',
                  'data' => [
              'confirm' => '¿Estas seguro de eliminar?',
              'method' => 'post',
          ],]) ?><?php endif; ?></td>
        </tr>
      </tbody>             
                     
      </table> 
    </table>
</div>
</div>

</li>

