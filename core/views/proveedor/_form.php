<?php
use app\models\Skill;
use app\models\Certificacion;
use app\models\Categoria;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\skillproveedor;
/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
    $this->registerJsFile( Yii::getAlias('@web').'/js/autocomplete.js', ['depends' => [\yii\web\JqueryAsset::className(), \yii\jui\JuiAsset::className(), \yii\bootstrap\BootstrapPluginAsset::className(), \yii\web\YiiAsset::className() ]]);
?>

<div class="proveedor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'razonsocial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contacto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefonocel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>


    <!-- CATEGORIAS -->
    <label>Categorias</label>
    <?php 
        $selectedData3= array();
        foreach ($model->categorias as $proveedorcategoria) {
            
            $selectedData3[] = $proveedorcategoria->categoria->Categoria;
        
        }
    ?>
    <textarea id="tags-categorias" name="tags-categorias"  data-items="<?php echo implode(',', $selectedData3); ?>"></textarea>

    <!-- FIN CATEGORIAS -->







 <!-- SKills -->

    <label>Skill</label><br>
    <?php 
        $selectedData= array();
        foreach ($model->skills as $skillproveedor) {
            
            $selectedData[] = $skillproveedor->skill->skill;
        
        }
    ?>
    <textarea id="tags-skills"  name="tags-skills"   data-items="<?php echo implode(',', $selectedData); ?>"></textarea>

    <!-- FIN skills -->






<!-- certificacion -->

    <label>Certificaciones</label><br>
    <?php 
        $selectedData2= array();
        foreach ($model->certificaciones as $certificacionesproveedor) {
            
            $selectedData2[] = $certificacionesproveedor->certificacion->certificacion;
        
        }
    ?>
    <textarea id="tags-certi" name="tags-certi" data-items="<?php echo implode(',', $selectedData2); ?>"></textarea>

    <!-- FIN  -->





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
