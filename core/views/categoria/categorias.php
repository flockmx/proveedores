<h2>Categorias</h2>

<?php foreach( $categorias as $categoria ): ?> 
	<button type="button" class="btn btn-comparativo btn-checkbox <?= (in_array( $categoria->id , $active))?'active':'' ?>" data-id="<?= $categoria->id ?>"><?= $categoria->Categoria ?></button>
<?php endforeach; ?>