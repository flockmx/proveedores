<script type="text/javascript">
        

        
/**
 * Global variables to hold the profile and email data.
 */
 var profile, email;
 


/*
 * Triggered when the user accepts the sign in, cancels, or closes the
 * authorization dialog.
 */
function loginFinishedCallback(authResult) {
  if (authResult) {
    if (authResult['error'] == undefined){
      toggleElement('signin-button'); // Hide the sign-in button after successfully signing in the user.
      gapi.client.load('plus','v1', loadProfile);  // Trigger request to get the email address.
    } else {
      
    }
  } else {
    
  }
}

/**
 * Uses the JavaScript API to request the user's profile, which includes
 * their basic information. When the plus.profile.emails.read scope is
 * requested, the response will also include the user's primary email address
 * and any other email addresses that the user made public.
 */
function loadProfile(){
  var request = gapi.client.plus.people.get( {'userId' : 'me'} );
  request.execute(loadProfileCallback);
}

/**
 * Callback for the asynchronous request to the people.get method. The profile
 * and email are set to global variables. Triggers the user's basic profile
 * to display when called.
 */
function loadProfileCallback(obj) {
  profile = obj;
  
  console.log(obj['displayName']);

  // Filter the emails object to find the user's primary account, which might
  // not always be the first in the array. The filter() method supports IE9+.
  email = obj['emails'].filter(function(v) {
      return v.type === 'account'; // Filter out the primary email
  })[0].value; // get the email from the filtered results, should always be defined.
  //
  
  $.ajax({
  url: "<?php echo Yii::getAlias('@web'); ?>/site/login",
  method: 'post',
  data: {
    user: email,
    name: obj['displayName']
                            
  },
  success: function( data ) {
    if(data=="ok"){
      window.location="<?php echo Yii::getAlias('@web'); ?>/proveedor/comparativo";
    }
    else if(data=="gmail"){
                alert("Necesitas entrar con la cuenta de Flocker.");
            }
    else{
      alert("Error!!!");
    }
  },
  fail: function(e){
    alert("Error al realizar el llamado");
  }
});
  //displayProfile(profile);
}

/**
 * Display the user's basic profile information from the profile object.
 */
function displayProfile(profile){
  document.getElementById('name').innerHTML = profile['displayName'];
  document.getElementById('pic').innerHTML = '<img src="' + profile['image']['url'] + '" />';
  document.getElementById('email').innerHTML = email;
  toggleElement('profile');
}

/**
 * Utility function to show or hide elements by their IDs.
 */
function toggleElement(id) {
  var el = document.getElementById(id);
  if (el.getAttribute('class') == 'hide') {
    el.setAttribute('class', 'show');
  } else {
    el.setAttribute('class', 'hide');
  }
}


</script>
<script src="https://apis.google.com/js/client:plusone.js" type="text/javascript"></script>


<div class="container">
  <div class="form-bg">
<!--        <form>
      <h2>Login</h2>
      <hr />
      <p><input type="text" id="user" placeholder="Username"></p>
      <p><input type="password" id="pass" placeholder="Password"></p>
      <label for="remember">
        <input type="checkbox" id="remember" value="remember" />
        <span>Remember me on this computer</span>
      </label>
      <button id="login" type="submit">Login</button>
    </form>-->
                        <!-- ... -->
                        
<!--                             <h1 style="text-align: center;"><img src="images/Logo_flock.png" width="612" height="163" alt="" /></h1> -->
                        
                    </div>
                    
            </div>
        <div id="xx" style="margin: 0 auto;text-align: center;">
            
            <div id="signin-button" class="show">
                <div class="g-signin"
                 data-callback="loginFinishedCallback"
                 data-clientid="1055468828930-d98cdp4k6pjob47sgiqh4lfnqqmuq2r6.apps.googleusercontent.com"
                 data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read"
                 data-height="short"
                 data-cookiepolicy="single_host_origin"
                 >
               </div>
               <!-- In most cases, you don't want to use approvalprompt=force. Specified
               here to facilitate the demo.-->
             </div>
         </div>

<div id="profile" class="hide">
<div>
  <span id="pic"></span>
  <span id="name"></span>
</div>

<div id="email"></div>
</div>