<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/textextjs/textext.core.css',
        'css/textextjs/textext.plugin.tags.css',
        'css/textextjs/textext.plugin.autocomplete.css',
        'css/textextjs/textext.plugin.arrow.css',
        'js/sliptree-bootstrap-tokenfield/dist/css/bootstrap-tokenfield.min.css',
        'js/sliptree-bootstrap-tokenfield/dist/css/tokenfield-typeahead.min.css',

    ];
    public $js = [
        'https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js',
        'js/sliptree-bootstrap-tokenfield/dist/bootstrap-tokenfield.min.js',
        'js/textextjs/textext.core.js',
        'js/textextjs/textext.plugin.tags.js',
        'js/textextjs/textext.plugin.autocomplete.js',
        'js/textextjs/textext.plugin.arrow.js',
        'js/textextjs/textext.plugin.ajax.js',
        'js/jquery.ui.touch-punch.min.js',


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset',
        
        
    ];
}
