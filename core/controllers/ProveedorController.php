<?php

namespace app\controllers;

use Yii;
use app\models\Proveedor;
use app\models\Certificacion;
use app\models\Skill;
use app\models\SkillProveedor;
use app\models\CertificacionesProveedor;
use app\models\ProveedorCategoria;
use app\models\ProveedorSerch;
use app\models\Categoria;
use app\models\Usuario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;

/**
 * ProveedorController implements the CRUD actions for Proveedor model.
 */
class ProveedorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['comparativo', 'proveedor'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function( $rule, $action ){
                            return Usuario::isUserAdmin( Yii::$app->user->id );
                        }
                    ]
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'comparativo' => ['post'],
                ],
            ],*/
        ];
    }



    public function actionProveedor(){

        $this->layout = false;

        $skills = array();
        if( isset( $_POST['skills'] ) ){
            $skills = $_POST['skills'];
        }

        $categorias = array();
        if( isset( $_POST['categorias'] ) ){
            $categorias = $_POST['categorias'];
        }

        if( empty( $skills ) && empty( $categorias ) ){
            $provedores = Proveedor::find()->orderBy('nombre')->all(); 

        }else if( empty( $skills ) ){
            // FILTRO DE CATEGORIAS

            $sql = 'SELECT * FROM Proveedor p WHERE p.id in ( select pc.id_proveedor from proveedorCategoria pc  where pc.id_categoria  in ( '.implode(',', $categorias).' )  ) order by p.nombre';
            $provedores = Proveedor::findBySql($sql)->all();

        }else if( empty( $categorias ) ){
            // FILTRO DE SKILLS

            $sql = 'SELECT * FROM Proveedor p WHERE p.id in ( select sc.id_proveedor from SkillProveedor sc where sc.id_skill  in ( '.implode(',', $skills).' ) )order by p.nombre';
           // $sql ->  orderBy('skill ASC'); 
            //->orderBy('id')
            $provedores = Proveedor::findBySql($sql)->all();


        }else{
            // FILTRO DE LOS DOS



            $sql = 'SELECT * FROM Proveedor p WHERE p.id in ( select sc.id_proveedor from SkillProveedor sc where sc.id_skill  in ( '.implode(',', $skills).' ) ) and p.id in ( select pc.id_proveedor from proveedorCategoria pc where pc.id_categoria in ( '.implode(',', $categorias).' ) ) order by p.nombre';
            $provedores = Proveedor::findBySql($sql)->all();
        }


        
        

        return $this->render('proveedor', [
            'provedores' => $provedores,
        ]);

    }

    /**
     * Lists all Proveedor models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new ProveedorSerch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionComparativo()
    {

        $skills = array();
        if( isset( $_GET['skills'] ) ){
            $skills = $_GET['skills'];
        }

        $categorias = array();
        if( isset( $_GET['categorias'] ) ){
            $categorias = $_GET['categorias'];
        }

        $name = '1 = 1';
        if( !empty( $_GET['nombre_proveedor'] ) ){
            $name = "nombre like '%".$_GET['nombre_proveedor']."%'";
        }


        if( empty( $skills ) && empty( $categorias ) ){
            $query = Proveedor::find()->where( $name );
        }else if( empty( $skills ) ){
            $query = Proveedor::find()->where( $name.' and Proveedor.id in ( select pc.id_proveedor from proveedorCategoria pc  where pc.id_categoria  in ( '.$categorias.' )  )' );
        }else if( empty( $categorias ) ){
            $query = Proveedor::find()->where( $name.' and Proveedor.id in ( select sc.id_proveedor from SkillProveedor sc where sc.id_skill  in ( '.$skills.' ) )');
        }else{
            $query = Proveedor::find()->where( $name.' and Proveedor.id in ( select sc.id_proveedor from SkillProveedor sc where sc.id_skill  in ( '.$skills.' ) ) and Proveedor.id in ( select pc.id_proveedor from proveedorCategoria pc where pc.id_categoria in ( '.$categorias.' ) )' );
        }





        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);



        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('nombre asc')
            ->all();

        return $this->render('comparativo', [
             'models' => $models,
             'pages' => $pages,
        ]);
    }

    



    /**
     * Displays a single Proveedor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proveedor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Proveedor();

        

        if( $model->load(Yii::$app->request->post()) ){

            $model->razonsocial = $_POST['Proveedor']['razonsocial'];
            $model->telefonocel = $_POST['Proveedor']['telefonocel'];

            if ( $model->save()){

                $this->updateRelations($model);
                return $this->redirect(['comparativo']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
        
            
    }

    private function updateRelations( $model ){

        /// SKILLS

        SkillProveedor::deleteAll(['id_proveedor' => $model->id]);
        if (isset($_POST["tags-skills"])){

            $skills = json_decode($_POST["tags-skills"]);


            foreach( $skills as $skill ){

                $s = Skill::find()->where( ['skill' => $skill] )->one();

                if( $s === null ){
                   $s = new Skill();

                   $s->skill = $skill;
                   $s->save();
                }


                $skillProveedor = new SkillProveedor();
                $skillProveedor->id_proveedor = $model->id;
                $skillProveedor->id_skill = $s->id;
                if (!$skillProveedor->save()) { 
                    var_dump($skillProveedor->getErrors());
                }

            }


        }
        /// FIN SKILLS




        /// CATEGORIAS

        ProveedorCategoria::deleteAll(['id_proveedor' => $model->id]);
        if (isset($_POST["tags-categorias"])){

            $categorias = json_decode($_POST["tags-categorias"]);


            foreach( $categorias as $categoria ){

                $c = Categoria::find()->where( ['Categoria' => $categoria] )->one();

                if( $c === null ){
                   $c = new Categoria();
                   $c->Categoria = $categoria;
                   $c->save();
                }

                $ProveedorCategoria = new ProveedorCategoria();
                $ProveedorCategoria->id_proveedor = $model->id;
                $ProveedorCategoria->id_categoria = $c->id;
                if (!$ProveedorCategoria->save()) { 
                    var_dump($ProveedorCategoria->getErrors());
                }

            }


        }


        /// FIN CATEGORIAS



        /// certificacion

        CertificacionesProveedor::deleteAll(['id_proveedor' => $model->id]);
        if (isset($_POST["tags-certi"])){

            $certificaciones = json_decode($_POST["tags-certi"]);


            foreach( $certificaciones as $certificacion ){

                $ce = Certificacion::find()->where( ['certificacion' => $certificacion] )->one();

                if( $ce === null ){
                   $ce = new Certificacion();
                   $ce->Certificacion = $certificacion;
                   $ce->save();
                }

                $CertificacionesProveedor = new CertificacionesProveedor();
                $CertificacionesProveedor->id_proveedor = $model->id;
                $CertificacionesProveedor->id_certificacion = $ce->id;
                if (!$CertificacionesProveedor->save()) { 
                    var_dump($CertificacionesProveedor->getErrors());
                }

            }


        }
            

        /// FIN certificacion
    }

    /**
     * Updates an existing Proveedor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if( $model->load(Yii::$app->request->post()) ){

            $model->razonsocial = $_POST['Proveedor']['razonsocial'];
            $model->telefonocel = $_POST['Proveedor']['telefonocel'];

            if ( $model->save()){

                $this->updateRelations($model);
                return $this->redirect(['comparativo']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);



    }

    /**
     * Deletes an existing Proveedor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {

    //CertificacionesProveedor::deleteAll(['id_proveedor' => $model->id]);
    //ProveedorCategoria::deleteAll(['id_proveedor' => $model->id]);
    //SkillProveedor::deleteAll(['id_proveedor' => $model->id]);  
       
    $model= $this->findModel($_GET["id"]); //busca el id del modelo proveedor
        CertificacionesProveedor::deleteAll(['id_proveedor' => $model->id]);
        ProveedorCategoria::deleteAll(['id_proveedor' => $model->id]);
        SkillProveedor::deleteAll(['id_proveedor' => $model->id]);  
        $model->delete();
        
        return $this->redirect(['comparativo']);

    }

    /**
     * Finds the Proveedor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proveedor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proveedor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
