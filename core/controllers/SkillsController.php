<?php

namespace app\controllers;

use Yii;
use app\models\Skill;
use app\models\SkillSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SkillsController implements the CRUD actions for Skill model.
 */
class SkillsController extends Controller
{/*
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
*/
 public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        //'actions' => ['comparativo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'comparativo' => ['post'],
                ],
            ],*/
        ];
    }





    public function actionSkills(){

        $this->layout = false;

        $active = array();
        if( isset( $_POST['skills'] ) ){
            $active = $_POST['skills'];
        }


        $categorias = array();
        if( isset( $_POST['categorias'] ) ){
            $categorias = $_POST['categorias'];
        }


        if( empty( $categorias ) ){
            $skills = Skill::find()->orderBy('Skill')->all();
        }else{
            $sql = 'select * from Skill where id in ( select id_skill from SkillProveedor where id_proveedor in ( select id_proveedor from proveedorCategoria where id_categoria in ('.implode(',', $categorias).') ) )order by skill';
            $skills = Skill::findBySql($sql)->all();
        }

        

        return $this->render('skills', [
            'Skill' => $skills,
            'active' => $active
        ]);

        


    }

    public function actionSkillsajax(){

        $this->layout = false;


        $skills = Skill::find()->select(['id','skill'])->orderBy('skill')->asArray()->all();    
        $response = array();

        foreach($skills as $skill){
            $response[] = array(
                'value' => $skill['id'],
                'label' => $skill['skill']
            );
        }

        echo json_encode( $response );
        

    }

    public function actionSkillsajax2(){

        $this->layout = false;


        $skills = Skill::find()->select(['id','skill'])->orderBy('skill')->asArray()->all();    
        $response = array();

        foreach($skills as $skill){
            $response[] = $skill['skill'];
        }

        echo json_encode( $response );
        

    }


    /**
     * Lists all Skill models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SkillSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Skill model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Skill model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Skill();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Skill model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Skill model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Skill model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Skill the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Skill::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
