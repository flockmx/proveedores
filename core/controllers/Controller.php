<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;




class SiteController extends Controller
{
	public function actionSay($message = 'hi')
	{
		return $this->render('say', ['message' => $message]);

	}

}
